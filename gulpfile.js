const { series, watch, src, dest } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps')
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const uglify = require('gulp-uglify');
const webp = require('gulp-webp');
const imagemin = require('gulp-imagemin');
const postcss = require('gulp-postcss');
const terser = require('gulp-terser');


//Déclaration des paths des dossiers
const paths = {
    images: {
        src: ['./src/img/**/*'],
        dest: './assets/img',
    },
    style: {
        src: ['./src/scss/style.scss'],
        dest: './assets/css',
    },
    js: {
        src: ['./node_modules/jquery/dist/jquery.min.js',
            './src/js/common.js'
        ],
        dest: './assets/js',
    },
    fonts: {
        src: ['./src/fonts/**/*'],
        dest: './assets/fonts',
    },
    video: {
        src: ['./src/video/**/*'],
        dest: './assets/img',
    },
    watch: {
        src: {
            js: './src/js/**/*.js',
            style: './src/scss/**/*.scss'
        }
    }
}


function buildStyles() {
    return src(paths.style.src)
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(paths.style.dest))
}


function buildJs() {
    return src(paths.js.src)
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(concat('common.min.js'))
        // .pipe(uglify()) //Retiré pour debug dev
        .pipe(terser())
        .pipe(sourcemaps.write('.'))
        .pipe(dest(paths.js.dest))
}

function buildFonts() {
    return src(paths.fonts.src)
        .pipe(dest(paths.fonts.dest))
}

function buildImg() {
    return src(paths.images.src)
        .pipe(imagemin())
        .pipe(dest(paths.images.dest))
}

function buildVideo() {
    return src(paths.video.src)
        .pipe(dest(paths.video.dest))
}

function watchFile() {
    watch(paths.watch.src.style, buildStyles);
    watch(paths.watch.src.js, buildJs);
}


exports.buildStyles = buildStyles;
exports.buildJs = buildJs;
exports.buildFonts = buildFonts;
exports.buildImg = buildImg;
exports.buildVideo = buildVideo;



exports.default = series(buildStyles, buildJs, buildFonts, buildImg, buildVideo);
exports.prod = series(buildStyles, buildJs, buildFonts, buildImg, buildVideo);
exports.dev = series(buildStyles, buildJs, buildFonts, buildImg, buildVideo, watchFile);